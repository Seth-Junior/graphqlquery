import React from 'react'
import { NativeBaseProvider } from 'native-base'
import CountryScreen from './src/screens/CountryScreen'

const App = () => {
  return (
    <NativeBaseProvider>
      <CountryScreen />
    </NativeBaseProvider>
  )
}

export default App
