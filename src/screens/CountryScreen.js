import React, { useState } from 'react'
import { SafeAreaView, ScrollView, StyleSheet, Text, View, CheckBox } from 'react-native'
import { ApolloClient, InMemoryCache, gql, useQuery } from '@apollo/client'
import { Checkbox, Divider, Heading, HStack } from 'native-base'

const client = new ApolloClient({
    cache: new InMemoryCache(),
    uri: 'https://countries.trevorblades.com'
});

const CountryScreen = () => {

    const [nameCheck, setNameCheck] = useState(true);
    const [codeCheck, setCodeCheck] = useState(true);
    const [phoneCheck, setPhoneCheck] = useState(true);
    const [emojiCheck, setEmojiCheck] = useState(true);
    const [languageCheck, setLanguageCheck] = useState(true);

    // write a GraphQL query that asks for names and codes for all countries
    const LIST_COUNTRIES = gql`
    query openQuery($countryName: Boolean!, $code: Boolean!,$phone: Boolean!, $emoji: Boolean!, $language: Boolean!){
        countries{
            name @include(if: $countryName)
            code @include(if: $code)
            phone @include(if: $phone)
            emoji @include(if: $emoji)
            languages{
                name @include(if: $language)
                }
            }
    }

    `;
    
    // const { data, loading, error } = useQuery(LIST_COUNTRIES, { client });
    const { data, loading, error } = useQuery(LIST_COUNTRIES, { client,
        variables: {
            countryName: nameCheck,
            code: codeCheck,
            phone: phoneCheck,
            emoji: emojiCheck,
            language: languageCheck
        }
     });

     
     if (loading || error) {
         return <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}><Text>{error ? error.message : 'Loading...'}</Text></View>
        }
        
        const countriesData = data?.countries.slice(0, 17)
        console.log("Country", countriesData);

    // console.log('Country', countriesData[0].languages[0].name);

    // if(nameCheck == false){
    //     console.log('False');
    // }else{
    //     console.log("True");
    // }

    return (
        <SafeAreaView>
            <View style={{ padding: 10 }}>
                <Heading mb={4}>Query By</Heading>
                <HStack style={{ justifyContent: 'space-between' }}>
                    <Checkbox
                        defaultIsChecked
                        value={nameCheck}
                        onChange={() => setNameCheck(!nameCheck)}
                        my={2}
                    >
                        Name
                    </Checkbox>
                    <Checkbox
                        defaultIsChecked
                        value={codeCheck}
                        onChange={() => setCodeCheck(!codeCheck)}
                        my={2}
                    >
                        Code
                    </Checkbox>
                    <Checkbox
                        defaultIsChecked
                        value={phoneCheck}
                        onChange={() => setPhoneCheck(!phoneCheck)}
                        my={2}
                    >
                        Phone
                    </Checkbox>
                </HStack>
                <HStack space={4} style={{ justifyContent: 'space-between' }}>
                    <Checkbox
                        defaultIsChecked
                        value={emojiCheck}
                        onChange={() => setEmojiCheck(!emojiCheck)}
                        my={2}
                    >
                        Emoji
                    </Checkbox>
                    <Checkbox
                        defaultIsChecked
                        value={languageCheck}
                        onChange={() => setLanguageCheck(!languageCheck)}
                        my={2}
                    >
                        Language
                    </Checkbox>
                    <Text></Text>
                </HStack>
            </View>
            <Divider marginY={3} />
            <ScrollView style={{ marginBottom: 20 }}>
                {
                    countriesData.map((coun, key) =>
                        <>
                            <View key={key} style={{ padding: 10 }}>
                                {coun.name ? <Text>Name : {coun.name}</Text> : null}
                                {coun.code ? <Text>Code : {coun.code}</Text> : null}
                                {coun.phone ? <Text>Phone : {coun.phone}</Text> : null}
                                {coun.emoji ? <Text>Emoji : {coun.emoji}</Text> : null}
                                {coun.languages[0]?.name? <Text>Language : {coun.languages[0]?.name}</Text> : null}
                            </View>
                            <Divider marginY={2} />
                        </>
                    )
                }
            </ScrollView>
        </SafeAreaView>
    )
}

export default CountryScreen

const styles = StyleSheet.create({
    container: {
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    checkbox: {
        alignSelf: "center",
    },
})
